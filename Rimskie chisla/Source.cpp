#include <iostream>
#include <fstream>
#include <cstring>


using namespace std;


char *Vrim(int arab)  
 {
 	const int arabs[] = { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };//�������� ����� 
 	const char *rims[] = { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };//������� �����

 	static char rim[20]; 


 	int n = sizeof(arabs) / sizeof(int) - 1;
 	int arabmax = arabs[n];//max ����� 1000 
	const char rimmax = rims[n][0];

 	int i = 0; 


 	while (arab>arabmax) //���� ����� ������ max ���������
 	{
 		rim[i++] = rimmax;
 		arab -= arabmax;
 	}


 	while (arab > 0)
 	{
 		if (arab >= arabs[n])
 		{
			rim[i++] = rims[n][0];
 			if (n & 1)
 				rim[i++] = rims[n][1];
 			arab -= arabs[n];
 		}
 		else
 			n--;
 	}


 	rim[i] = 0;
 	return rim; 
 }


 int main()
 {
 	char str[100];


 	ifstream in("in.txt");
 	if (!in.is_open())
 	{
 		cout << "Read file Error!" << endl;//��������
 		system("pause");
 		return 1;
 	}


 	ofstream out;
 	out.open("out.txt");
 	if (!out.is_open())
 	{
 		cout << "Write file Error!" << endl;//�������
 		system("pause");
 		return 1;//
 	}


 	while (!in.eof())//���� ���� �� ����������
 	{
 		in.getline(str, 100);
 		char *context;
 		char *sl = strtok_s(str, " ", &context);


 		while (sl != NULL)//�� ����� ������
 		{
 			if (strlen(sl) == 10 && sl[2] == '.' && sl[5] == '.')//���� �� 10 ��������
 			{
 				char day[3] = { sl[0], sl[1], '\0' };
 				char month[3] = { sl[3], sl[4],'\0' };
 				char year[5] = { sl[6], sl[7], sl[8], sl[9], '\0' };


 				int arab = atoi(year);


 				if (atoi(day) <= 31 && atoi(month) <= 12 && arab != 0)//�������� �� 0 
 				{
 					char *rim;
 					rim = Vrim(arab);
 					out << day << "." << month << "." << rim << " ";
					cout << "DONE" << endl;
 				}
 				else
 					out << sl << " ";//���� �� ���� 
 			}
 			else
 				out << sl << " ";
 			sl = strtok_s(NULL, " ", &context);
 		}
 		out << endl;
 	}


 	out.close(); 
 	in.close();


 	system("pause");
 	return 0;
 }
